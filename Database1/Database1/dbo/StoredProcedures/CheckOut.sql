﻿CREATE PROCEDURE [dbo].[CheckOut]
	@UserId int,
	@ProjectId int,
	@Filename nvarchar(200)
AS
	SELECT
		a.Id,
		a.Name as Filename,
		a.FileData as Data,
		b.Name as ProjectName,
		c.Username as Username
	FROM
		Files a
		INNER JOIN Projects b on ISNULL(a.ProjectId, 0) = ISNULL(b.Id, 0)
		INNER JOIN Users c on b.UserId = c.Id
	WHERE
		a.Name = @Filename
		AND b.Id = @ProjectId
		AND c.Id = @UserId

	IF (@@ROWCOUNT > 0)
	BEGIN
		UPDATE a
		SET a.CheckedOutBy = c.Id
		FROM
			Files a
			INNER JOIN Projects b on a.ProjectId = b.Id
			INNER JOIN Users c on b.UserId = c.Id
		WHERE
			a.Name = @Filename
			AND ISNULL(b.Id, 0) = ISNULL(@ProjectId, 0)
			AND c.Id = @UserId
	END
		
RETURN 0
