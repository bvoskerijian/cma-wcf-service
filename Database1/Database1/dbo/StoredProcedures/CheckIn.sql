﻿CREATE PROCEDURE [dbo].[CheckIn]
	@UserId int,
	@ProjectId int,
	@Filename nvarchar(200),
	@Data varbinary(max)
AS
	IF EXISTS (
		SELECT a.Id 
		FROM 
			Files a
			INNER JOIN Projects b on a.ProjectId = b.Id
			INNER JOIN Users c on b.UserId = c.Id
		WHERE 
			c.Id = @UserId
			AND b.Id = @ProjectId
			AND a.Name = @Filename
	)
	BEGIN
		-- This file already exists for this user, mark as checked in
		UPDATE a
		SET CheckedOutBy = NULL
		FROM 
			Files a
			INNER JOIN Projects b on a.ProjectId = b.Id
			INNER JOIN Users c on b.UserId = c.Id
		WHERE 
			c.Id = @UserId
			AND b.Id = @ProjectId
			AND a.Name = @Filename
	END
	ELSE
	BEGIN
		INSERT INTO Files(Name, ProjectId, FileData)
		VALUES(@Filename, @ProjectId, @Data)

		RETURN @@IDENTITY
	END
RETURN 0
