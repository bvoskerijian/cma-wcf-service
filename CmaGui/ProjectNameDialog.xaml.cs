﻿/////////////////////////////////////////////////////////////////////////
// ProjectNameDialog.xaml.cs                                           //
//                                                                     //
//                                                                     //
//                                                                     //
// Brian Voskerijian, CSE681-Software Modeling & Analysis, Spring 2018 //
/////////////////////////////////////////////////////////////////////////
//
// - Implements the project name dialog
// - This has the sole purpose of getting the project name to create
/////////////////////////////////////////////////////////////////////////

using System.Windows;

namespace CmaGui
{
  /// <summary>
  /// Interaction logic for ProjectNameDialog.xaml
  /// </summary>
  public partial class ProjectNameDialog : Window
  {
    public string ProjectName { get; set; }
    public ProjectNameDialog()
    {
      InitializeComponent();
    }

    private void CreateButton_Click(object sender, RoutedEventArgs e)
    {
      ProjectName = ProjectNameTextBox.Text;
      Close();
    }

    private void CancelButton_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }
  }
}
