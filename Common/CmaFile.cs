﻿/////////////////////////////////////////////////////////////////////////
// CmaFile.cs                                                          //
//                                                                     //
//                                                                     //
//                                                                     //
// Brian Voskerijian, CSE681-Software Modeling & Analysis, Spring 2018 //
/////////////////////////////////////////////////////////////////////////
//
// - Implements a domain object for CMA files
/////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class CmaFile
    {
      public int Id { get; set; }
      public string Name { get; set; }
      public byte[] Data { get; set; }
      public string ProjectName { get; set; }
      public string Username { get; set; }
  }
}
