﻿/////////////////////////////////////////////////////////////////////////
// IService.cs - Interface for BasicService demo                       //
//                                                                     //
// Jim Fawcett, CSE681 - Software Modeling and Analysis, Fall 2010     //
/////////////////////////////////////////////////////////////////////////
//
// - Started with C# Class Library Project
// - Made reference to .Net System.ServiceModel
// - Added using System.ServiceModel
/////////////////////////////////////////////////////////////////////////
// This file has been adapted for use by Brian Voskerijian
/////////////////////////////////////////////////////////////////////////

using Common;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;

namespace RemoteRepositoryService
{
  [ServiceContract(Namespace= "RemoteRepositoryService")]
  public interface IRemoteRepositoryService
  {
    // Creates a user if one does not already exist
    [OperationContract]
    bool CreateUser(string role, string username);

    // Creates a project if one does not already exist
    [OperationContract]
    bool CreateProject(string username, string projectName);

    // Gets the specified user if they exist
    [OperationContract]
    User GetUser(string username);

    // Returns a list of projects under the specified user
    [OperationContract]
    IEnumerable<string> BrowseProjects(string username);

    // Returns a list of files under the specified project
    [OperationContract]
    IEnumerable<CmaFile> BrowseFiles(string username, string project); 

    // Shares a project with another user
    [OperationContract]
    bool ShareProject(string username, string project);

    // Checks in the specified file
    [OperationContract(IsOneWay = true)]
    void CheckIn(CheckInData data);

    // Checks out the specified file
    [OperationContract]
    Stream CheckOut(string username, string filename, string project);
  }
}
