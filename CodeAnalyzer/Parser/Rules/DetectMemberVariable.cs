﻿using System.Collections.Generic;
using System.Linq;
using Parser.Parser;

/*
 * DetectMemberVariable
 *
 * This class implements a rule to detect member variables. This rule will be used
 * to save member variables in each class to calculate cohesion. This is to be run in the
 * first pass of the parser to collect data required in the second pass.
 *
 */
namespace Parser.Rules
{
  class DetectMemberVariable : ARule
  {
    private readonly Repository _repo;

    public DetectMemberVariable(Repository repo)
    {
      _repo = repo;
    }

    public override bool test(CSemiExp semi)
    {
      // Must be in class
      // Must have access modifier (public, protected, etc.)
      // Is not a function (put after function detector)
      var accessModifiers = new List<string> { "public", "protected", "private", "internal" };
      var mutationModifier = new List<string> { "readonly", "const" };

      var currentClassName = _repo.stack.GetContext(e => e.type == "class")?.name;
      if (currentClassName == null)
      {
        // We're not inside a class
        return false;
      }

      var containsAccessModifier = accessModifiers.Any(a => semi.Contains(a) != -1);
      var containsParens = semi.Contains("(") != -1 || semi.Contains(")") != -1;
      if (containsAccessModifier && !containsParens)
      {
        var local = new CSemiExp();
        local.Add(currentClassName);
        if (semi.Contains("=") == -1)
        {
          // Second to last token is the name of the member variable
          local.Add(semi[semi.count - 2]);
          doActions(local);
          return true;
        }
        else
        {
          // This member is being assigned, it is before the equals sign
          var index = semi.FindFirst("=");
          local.Add(semi[index - 1]);
          doActions(local);
          return true;
        }
      }

      return false;
    }
  }
}
