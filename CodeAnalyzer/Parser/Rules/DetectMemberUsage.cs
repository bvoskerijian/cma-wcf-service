﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Parser;

/*
 * DetectMemberUsage
 *
 * This class implements a rule to detect the usage of member variables in each funtion.
 * This rule will be used to save member variables in each class to calculate cohesion. It
 * requires a list of class members to be passed into the constructor. This will be obtained
 * on the first pass of the parser.
 *
 */
namespace Parser.Rules
{
  public class DetectMemberUsage : ARule
  {
    private readonly Repository _repo;
    private readonly Dictionary<string, List<string>> _classMembers;

    public DetectMemberUsage(Repository repo, Dictionary<string, List<string>> classMembers)
    {
      _repo = repo;
      _classMembers = classMembers;
    }

    public override bool test(CSemiExp semi)
    {
      var currentClassName = _repo.stack.GetContext(e => e.type == "class")?.name;
      var currentFunctionName = _repo.stack.GetContext(e => e.type == "function")?.name;
      if (currentClassName == null || currentFunctionName == null)
      {
        // We're not inside a class method
        return false;
      }

      if (!_classMembers.ContainsKey(currentClassName))
      {
        // This class has no members
        return false;
      }

      var classMembers = _classMembers[currentClassName];
      foreach (var member in classMembers)
      {
        var tokenIndex = semi.FindFirst(member);
        if (tokenIndex != -1)
        {
          var local = new CSemiExp();
          local.Add(currentClassName);
          local.Add(currentFunctionName);
          local.Add(member);
          doActions(local);
          return false;
        }
      }


      return false;
    }
  }
}
