﻿using System.Collections.Generic;
using Common;

namespace LocalRepository
{
    public interface IRepository
    {
        void AddUser(User user);
        void CheckIn(string username, string projectName, string filename, byte[] fileData);
        CmaFile CheckOut(string username, string projectName, string filename);
        void ClearAll();
        void ClearFiles();
        void ClearProjects();
        void ClearUsers();
        int CreateProject(string username, string projectName);
        IEnumerable<CmaFile> GetFiles(string username, string project);
        int GetProjectId(string username, string name);
        IEnumerable<string> GetProjects(string username);
        User GetUser(string username);
        int GetUserId(string name);
        IEnumerable<User> GetUsers();
        bool UserExists(string username);
    }
}