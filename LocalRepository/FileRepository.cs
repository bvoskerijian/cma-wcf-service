﻿using System;
using System.Collections.Generic;
using Common;
using System.IO;
using System.Linq;

namespace LocalRepository
{
    public class FileRepository : IRepository
    {
        private readonly DirectoryInfo _root;
        private readonly string _usersFilePath;

        public FileRepository(string rootDirectoryName)
        {
            // Create root directory on the C: drive
            _root = Directory.CreateDirectory(rootDirectoryName);
            _usersFilePath = Path.Combine(_root.FullName, "users");
        }
        public void AddUser(User user)
        {
            using (var fs = new StreamWriter(_usersFilePath))
            {
                fs.WriteLine($"{user.Username}, {user.Role}");
            }
        }

        public void CheckIn(string username, string projectName, string filename, byte[] fileData)
        {
            throw new NotImplementedException();
        }

        public CmaFile CheckOut(string username, string projectName, string filename)
        {
            throw new NotImplementedException();
        }

        public void ClearAll()
        {
            throw new NotImplementedException();
        }

        public void ClearFiles()
        {
            throw new NotImplementedException();
        }

        public void ClearProjects()
        {
            throw new NotImplementedException();
        }

        public void ClearUsers()
        {
            File.WriteAllText(_usersFilePath, string.Empty);
        }

        public int CreateProject(string username, string projectName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CmaFile> GetFiles(string username, string project)
        {
            throw new NotImplementedException();
        }

        public int GetProjectId(string username, string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetProjects(string username)
        {
            throw new NotImplementedException();
        }

        public User GetUser(string username)
        {
            try
            {
                return File.ReadAllLines(_usersFilePath)
                    .Select(line => new User
                    {
                        Username = line.Split(',')[0],
                        Role = line.Split(',')[1]
                    })
                    .FirstOrDefault(u => string.Compare(u.Username, username, StringComparison.CurrentCultureIgnoreCase) == 0);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public int GetUserId(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public bool UserExists(string username)
        {
            throw new NotImplementedException();
        }
    }
}
